﻿using UnityEngine;

namespace Assets.Utils
{
    class JsonMeshTester: MonoBehaviour
    {
        public Mesh mesh;
        public MeshFilter filter;

        public string str;


        private void OnGUI()
        {
            if(GUI.Button(new Rect(10,10,100,30), "Save"))
            {
                JsonMesh jsonMesh = new JsonMesh();
                jsonMesh.mesh = this.mesh;
                this.str = JsonUtility.ToJson(jsonMesh, true);
            }

            if(GUI.Button(new Rect(10,50,100,30), "Clear"))
            {
                this.mesh = null;
                this.filter.mesh = this.mesh;
            }

            if(GUI.Button(new Rect(10,90, 100,30), "Load"))
            {
                JsonMesh jsonMesh = JsonUtility.FromJson<JsonMesh>(this.str);
                this.filter.mesh = jsonMesh.mesh;
            }
        }
    }
}
