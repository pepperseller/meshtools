﻿using UnityEngine;
using System.Collections.Generic;

namespace Assets.Utils
{
    /// <summary>
    /// serializes a mesh from and to json
    /// </summary>
    [System.Serializable]
    class JsonMesh: ISerializationCallbackReceiver
    {
        //----member fields----//
        /// <summary>
        /// mesh to be serialized to or from
        /// </summary>
        public Mesh mesh;

        //--serialization fields--//
        [HideInInspector] [SerializeField] private List<Vector3> _verts;
        [HideInInspector] [SerializeField] private List<Vector3> _normals;
        [HideInInspector] [SerializeField] private List<int> _triangles;
        [HideInInspector] [SerializeField] private List<Vector2> _uvs;

        //----methods----//
        /// <summary>
        /// called when loading FROM json
        /// </summary>
        public void OnAfterDeserialize()
        {
            this.mesh = new Mesh();

            this.mesh.SetVertices(this._verts);
            this.mesh.SetNormals(new List<Vector3>(this._normals));
            this.mesh.SetTriangles(this._triangles, 0);
            this.mesh.SetUVs(0, new List<Vector2>(_uvs));

            this.mesh.RecalculateBounds();

        }

        /// <summary>
        /// called when saving TO json
        /// </summary>
        public void OnBeforeSerialize()
        {
            Debug.Log("Serializeing! " + this.mesh.name + " " + this.mesh.vertexCount +"verts");

            this._verts = new List<Vector3>(mesh.vertices);
            this._normals = new List<Vector3>(mesh.normals);
            this._triangles = new List<int>(mesh.triangles);
            this._uvs = new List<Vector2>(mesh.uv);

        }


    }
}
